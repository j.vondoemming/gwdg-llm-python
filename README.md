# GWDG LLM Python Library (chatai)

[![Latest Release](https://gitlab.gwdg.de/j.vondoemming/gwdg-llm-python/-/badges/release.svg)](https://gitlab.gwdg.de/j.vondoemming/gwdg-llm-python/-/releases)

This is an **UNOFFICIAL** python library to interact with the [GWDG LLM Service](https://chat-ai.academiccloud.de/).

![Demo of ./examples/interactive_chat.py](demo.gif)

## Getting started

1. Install the library via pip:
    ```bash
    pip3 install --index-url https://gitlab.gwdg.de/api/v4/projects/36289/packages/pypi/simple chatai
    ```
    Alternativly you can add the following two lines to your `requirements.txt` file and run `pip3 install -r requirements.txt`:
    ```
    --extra-index-url https://gitlab.gwdg.de/api/v4/projects/36289/packages/pypi/simple
    chatai
    ```
    (For other installation methods and downloads, see [here](https://gitlab.gwdg.de/j.vondoemming/gwdg-llm-python/-/packages/1556).)
2. Import `ChatAI` and create a session:
    ```python3
    from chatai.chatai import get_token, ChatAI, Model
    session = ChatAI(token=get_token("your username", "your password"), model=Model.INTEL_NEURAL_CHAT_7B)
    ```
3. Send messages and receive responses:
    ```python3
    print(session.send("Hello, I am Jake."))
    # Hi Jake, it's nice to meet you. Feel free to ask me anything or have a conversation.
    print(session.send("What is my name?"))
    # Your name is Jake.
    ```

Instead of authenticating with username and password you can also get a token manually:
    1. Go to [https://chat-ai.academiccloud.de/](https://chat-ai.academiccloud.de/).
    2. Login via Academic Cloud.
    3. Right-click on the page → Inspect (Q) → Storage
    4. There should be a cookie named `mod_auth_openidc_session` with a value in the form of `XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX`. That value is your token.

## What is this?

The GWDG is [hosting a LLM Service since February 2024](https://info.gwdg.de/news/en/gwdg-llm-service-generative-ai-for-science/).
This library here is to interact with that service via code.

## Examples

You can find examples in the [examples/ directory](./examples/)

## Documentation

The full library is documented with sphinx style docstrings.
It should work just fine, if you use an editor with a python language server.

Alternatively you can either [look in the source code](./chatai/chatai.py), as it is really short.

Or you can execute `make docs` or `make serve-docs` to generate the documentation as HTML.

## Technical details

This whole library is just a simple wrapper around [gradio-client](https://www.gradio.app/guides/getting-started-with-the-python-client).

