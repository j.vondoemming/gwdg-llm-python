
from datetime import datetime
from typing import Callable, List, Literal, TypedDict, cast
from gradio_client import Client
from enum import Enum
import requests
from bs4 import BeautifulSoup

from gradio_client.client import Job

class Message(TypedDict):
    """
    In case role is ASSISTANT: The timestamp of when this Message object was created. If you want to know when the message of the assistant was fully received, you need to look at the job.
    In case role is USER: The timestamp of when this Message object was created and thus send.
    """
    timestamp: datetime
    """
    If this is a message by the user or the ai model.
    """
    role: Literal['USER', 'ASSISTANT']
    """
    In case role is ASSISTANT: If message is None, it means the model is still generating the response. Message will automatically be set once the job is finished.
    In case role is USER: The message.
    """
    message: str | None
    """
    In case role is ASSISTANT: Which model was used to generate this message.
    In case role is USER: None
    """
    model: str | None
    """
    In case role is ASSISTANT: The job that was/is receiving the message.
    In case role is USER: The job of the Message object that was/is receiving the response to this message.
    """
    job: Job



class Model(Enum):
    """
    The ai models the GWDG LLM Service supports, as of 12.03.2024.
    """

    """
    Intel Neural Chat 7B
    """
    INTEL_NEURAL_CHAT_7B = 'Intel Neural Chat 7B'

    """
    Mixtral 8x7B Instruct
    """
    MIXTRAL_8X7B_INSTRUCT = 'Mixtral 8x7B Instruct'

    """
    Qwen1.5 72B Chat
    """
    QWEN15_72B_CHAT = 'Qwen1.5 72B Chat'

    """
    OpenAI GPT-3.5-Turbo (External)
    """
    OPENAI_GPT_35_TURBO_EXTERNAL = 'OpenAI GPT-3.5-Turbo (External)'

    """
    OpenAI GPT-4 (External)
    """
    OPENAI_GPT_4_EXTERNAL = 'OpenAI GPT-4 (External)'


class AuthenticationException(Exception):
    pass


"""
Get an authentication token for use with the ChatAI constructor using a username and password.
"""
def get_token(username: str, password: str, _url: str = "https://chat-ai.academiccloud.de/"):
    with requests.Session() as session:

        def submit_form(form, **kwargs):
            url = kwargs.get("url", form["action"])
            data = kwargs.get("data", {})
            prefilled_data = {
                input.get("name"): input.get("value")
                for input in form.find_all("input")
            }
            return session.post(url, prefilled_data | data)

        chatai_response = session.get(_url)

        chatai_page = BeautifulSoup(chatai_response.text, "html.parser")
        sso_response = submit_form(chatai_page.form)

        sso_page = BeautifulSoup(sso_response.text, "html.parser")
        sso_after_username_input = submit_form(
            sso_page.find("form", {"class": "login"}),
            url=sso_response.url,
            data={
                "login": username,
                "keep_signed_in": "Yes",
            },
        )

        sso_password_page = BeautifulSoup(sso_after_username_input.text, "html.parser")
        if error_message := sso_password_page.find("div", {"class": "error-msg"}):
            raise AuthenticationException(error_message.text.strip())

        sso_postlogin = submit_form(sso_password_page.form, data={"password": password})

        sso_postlogin_page = BeautifulSoup(sso_postlogin.text, "html.parser")
        if error_message := sso_postlogin_page.find("div", {"class": "error"}):
            raise AuthenticationException(error_message.text.strip())

        submit_form(sso_postlogin_page.form)

        return session.cookies["mod_auth_openidc_session"]


class ChatAI(object):

    def __init__(self, token: str, model: Model | str = Model.INTEL_NEURAL_CHAT_7B, verbose: bool = False, _url: str = "https://chat-ai.academiccloud.de/"):
        """
        Create a new ChatAI session.

        Messages send in a session will be remembered for that session and used as context in future messages.

        :param token: The mod_auth_openidc_session cookie value on https://chat-ai.academiccloud.de/.
        :param model: The ai model that should be used to generate responses.
        :param verbose: Whether the internal client should print output to the console.
        """

        self.verbose = verbose
        self.url = _url

        self.switch_model(model)

        # Connect to the Gradio APP.
        headers = {
            "Cookie": "mod_auth_openidc_session=" + token
        }
        self.client = Client(src=self.url, verbose=self.verbose, headers=headers, upload_files=False)

        self.history: List[Message] = []

    def view_api(self, print_info: bool = True, all_endpoints: bool = False) -> dict:
        """
        View the usage info of the Gradio API of https://chat-ai.academiccloud.de/.
        This library may break if the API changes.

        :param print_info: Whether the info should be printed in a human readable format to the console.
        :param all_endpoints: Whether all endpoints should be taken into account or just the named ones.
        :return: The usage information in a machine readable format.
        """
        res =  self.client.view_api(all_endpoints=all_endpoints, print_info=print_info, return_format="dict")
        res = cast(dict, res)
        return res

    def get_history(self) -> List[Message]:
        """
        Get the history of messages in this session.
        Note that this is just a local record of the messages.
        The server also keeps track of the messages.
        The servers messages and those in this local record may get out of sync.
        Keep that in mind.

        :return The messages:
        """
        return self.history

    def reset(self):
        """
        Reset the session.
        This will cause the model to forget your previous messages in this session.
        Note: The messages may still linger on the server for some time.
        """
        self.history.clear()
        self.client.reset_session()
        pass

    def switch_model(self, model: Model | str):
        """
        Change which ai model should be used to generate future responses.
        Note: The new model will also have access to the previous messages in this session.

        :param model: The name of the model.
        """
        if isinstance(model, Model):
            model = str(model.value)
        self.model = model

    def send(self, msg: str) -> str:
        """
        Send a message in the chat and get the response.
        This function is blocking and may take a couple of seconds until the entire response is received.

        :param msg: The message you want to send.
        :return: The models answer.
        """
        #result = self.client.predict(
        #    msg, # str  in 'Message' Textbox component
        #    self.model, # Literal['Intel Neural Chat 7B', 'Mixtral 8x7B Instruct', 'Qwen1.5 72B Chat', 'OpenAI GPT-3.5-Turbo (External)', 'OpenAI GPT-4 (External)']  in 'parameter_2' Dropdown component
        #    api_name="/chat"
        #)
        return self.send_async(msg=msg).result()

    def send_async(self, msg: str, result_callbacks: Callable | List[Callable] | None = None) -> Job:
        """
        Send a message in the chat asynchronously.
        This function is non-blocking.

        This function is useful if you want to implement one of those cool animations showing the partial response coming in as the model generates it.

        Example:
            job = session.send_async("Write a flask webserver for me that can handle file uploads.")
            print(job.status())
            for partial in job:
                print(partial)
            print(job.status())

        :param msg: The message you want to send.
        :param result_callbacks: A callback or a list of callbacks that will be called once the full response is available.
        :return: A Gradio Job that will accumulate the response of the model over time and eventually contain the full response.
        """
        msg = str(msg) # Ensure the message is a "copy" and not a reference.
        model = str(self.model) # Ensure the model name is a "copy" and not a reference.

        usermsg = cast(Message, {
            'timestamp': datetime.now(),
            'role': "USER",
            'message': msg,
            'job': None, # Will be set before this function returns.
            'model': None,
        })

        respmsg = cast(Message, {
            'timestamp': datetime.now(),
            'role': "ASSISTANT",
            'message': None,
            'job': None, # Will be set before this function returns.
            'model': model,
        })

        # Ensure result_callbacks is a list.
        if result_callbacks is None:
            result_callbacks = []
        elif not isinstance(result_callbacks, list):
            result_callbacks = [result_callbacks]

        # Make sure the history is correct once the full response is received.
        result_callbacks = [lambda m: respmsg.update({
            'message': m # The final message.
        })] + result_callbacks

        # Send the users message to the assistant and create a job for receiving the response.
        job = self.client.submit(
            msg, # str  in 'Message' Textbox component
            model, # Literal['Intel Neural Chat 7B', 'Mixtral 8x7B Instruct', 'Qwen1.5 72B Chat', 'OpenAI GPT-3.5-Turbo (External)', 'OpenAI GPT-4 (External)']  in 'parameter_2' Dropdown component
            api_name="/chat",
            result_callbacks=result_callbacks
        )

        # Append the messages to the history.
        usermsg['job'] = job
        respmsg['job'] = job
        self.history.append(usermsg)
        self.history.append(respmsg) # May not be fully received yet.

        return job


