.PHONY: default
default: test

.PHONY: test
test: venv/bin/activate install
	mkdir -p reports
	. venv/bin/activate && \
		python -m xmlrunner discover -o ./reports projectpi.tests
		#python3 -m unittest discover projectpi.tests

.PHONY: install
install: venv/bin/activate
	. venv/bin/activate && \
		python3 -m pip install .

.PHONY: dist
dist: build

.PHONY: build
build: venv/bin/activate
	. venv/bin/activate && \
		python3 -m build

.PHONY: clean
clean:
	$(RM) -r venv/ build/ dist/ projectpi.egg-info/
	find fissg/ -type d -name '__pycache__' -exec rm -rf {} +

venv/bin/activate: requirements.txt pyproject.toml
	python3 -m venv venv
	. venv/bin/activate && \
		python3 -m pip install -r requirements.txt


