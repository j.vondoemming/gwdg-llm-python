#!/usr/bin/env python3


from chatai.chatai import ChatAI, Model, get_token
from gradio_client.client import Status
import os
import time
from getpass import getpass

class COLORS:
    USER = '\033[92m'
    ASSISTANT = '\033[94m'
    QUESTION = '\033[95m'
    RESET = '\033[0m'

def ask_for_model() -> str:
    models = list(enumerate(sorted([e.value for e in Model])))
    print("Available models:")
    for index, model in models:
        print(f"[{index}]: {model}")
    while True:
        sel = input(f"{COLORS.QUESTION}Select model: {COLORS.RESET}")
        try:
            return models[int(sel)][1]
        except:
            pass

def print_response(job):
    while job.status().code is Status.STARTING:
        time.sleep(0.1) # Wait for job to begin.
    # Print the response as running text
    alreadygenerated = 0
    while not job.done():
        outputs = job.outputs()
        if len(outputs) == 0:
            outputs = [""]
        msg = outputs[-1] # Partial message
        print(msg[alreadygenerated:], end='', flush=True) # Print only the new characters of the message
        alreadygenerated = len(msg) # How many characters have already been generated
        time.sleep(0.02)
    print() # Newline

def ask_for_username_and_password():
    username = input("username: ")
    password = getpass(prompt="password: ")
    return get_token(username, password)

# Read token from environment.
token = os.environ.get('CHATAI_TOKEN') or ask_for_username_and_password()

# Ask the user which model should be used.
model = ask_for_model()
print("Selected model:", model)

# Begin the session.
session = ChatAI(token, model=model)

# Start the chat.
print("Type 'quit' to quit.")
while True:
    msg = input(f"{COLORS.USER}User> {COLORS.RESET}")
    if msg.lower() == "quit":
        exit(0)
    job = session.send_async(msg)
    print(f"{COLORS.ASSISTANT}Assistant> {COLORS.RESET}", end='')
    print_response(job)




