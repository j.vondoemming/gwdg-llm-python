#!/usr/bin/env python3

import os
from chatai.chatai import ChatAI, Model

token = os.environ['CHATAI_TOKEN']
session = ChatAI(token, model=Model.INTEL_NEURAL_CHAT_7B)

print(session.send("Hello, I am Jake."))
# Prints: Hello Jake, it's nice to meet you. Feel free to ask any questions or discuss any topics you might be interested in. I'm here to assist you and make your experience more enjoyable.

print(session.send("What is my name?"))
# Prints: Your name is Jake.
