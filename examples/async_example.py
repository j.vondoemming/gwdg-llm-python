#!/usr/bin/env python3

import os

from gradio_client.client import Status
from chatai.chatai import ChatAI, Model
import time

token = os.environ['CHATAI_TOKEN']
session = ChatAI(token, model=Model.INTEL_NEURAL_CHAT_7B)
job = session.send_async("Write a flask webserver in python that can handle file uploads.")

while job.status().code is Status.STARTING:
    time.sleep(0.1) # Wait for job to begin.

# Print the response as running text
alreadygenerated = 0
while not job.done():
    msg = job.outputs()[-1] # Partial message
    print(msg[alreadygenerated:], end='', flush=True) # Print only the new characters of the message
    alreadygenerated = len(msg) # How many characters have already been generated
    time.sleep(0.02)


